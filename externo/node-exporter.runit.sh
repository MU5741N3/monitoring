#!/bin/bash
############################################
#                                          #
#              Node Exporter               #
#                                          #
############################################

NODE_EXPORTER_CONTAINER="node-exporter"
NODE_EXPORTER_PORT="5026"

#Enable this opcions in Centos
#firewall-cmd --permanent --add-port=$NODE_EXPORTER_PORT/tcp
#firewall-cmd --reload

docker run -itd --name $NODE_EXPORTER_CONTAINER \
    -p $NODE_EXPORTER_PORT:9100 \
    --cap-add=SYS_TIME \
    --net="host" \
    --pid="host" \
    -v "/:/node-exporter:ro" \
    quay.io/prometheus/node-exporter --path.rootfs=/node-exporter
