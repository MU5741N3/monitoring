#!/bin/bash
############################################
#                                          #
#                Prometheus                #
#                                          #
############################################

PROMETHEUS_CONTAINER="prometheus"
PROMETHEUS_PORT="9090"
PROMETEHUS_ALIAS_MONITORING="rruv-monitoring"
PROMETHEUS_IP_LOCAL_SERVER="192.168.1.67"
PROMETHEUS_IP_LOCAL_NODE_EXPORTER="192.168.1.67"
PROMETHEUS_IP_LOCAL_CADVISOR="192.168.1.67"
PROMETHEUS_IP_EXTERNAL_NODE_EXPORTER="187.189.94.240"
PROMETHEUS_IP_EXTERNAL_CADVISOR="187.189.94.240"

#Enable this opcions in Centos
#firewall-cmd --permanent --add-port=$PROMETHEUS_PORT/tcp
#firewall-cmd --reload



cat<<-EOF > /Users/mu5t41n3/Documents/docker-compose/Prometheus/archivosBase/prometheus/prometheus.yml
global:
    scrape_interval: 3s
    external_labels:
        monitor: '$PROMETEHUS_ALIAS_MONITORING'
scrape_configs:
    - job_name: 'prometheus'
      static_configs:
        - targets: ['$PROMETHEUS_IP_SERVER:$PROMETHEUS_PORT']
    - job_name: 'monitoring-local'
      static_configs:
          - targets: ['$PROMETHEUS_IP_LOCAL_NODE_EXPORTER:9100','$PROMETHEUS_IP_LOCAL_CADVISOR:8080']
    - job_name: 'monitoring-external'
      static_configs:
          - targets: ['$PROMETHEUS_IP_EXTERNAL_NODE_EXPORTER:9100','$PROMETHEUS_IP_EXTERNAL_CADVISOR:5025']
EOF

docker run -itd --name $PROMETHEUS_CONTAINER \
    -p $PROMETHEUS_PORT:9090 \
    -v /Users/mu5t41n3/Documents/docker-compose/Prometheus/archivosBase/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml:z \
    -v /etc/localtime:/etc/localtime:ro \
    -v /usr/share/zoneinfo:/usr/share/zoneinfo:ro \
    -e "TZ=America/Mexico_City" \
    prom/prometheus
