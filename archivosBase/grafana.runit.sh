#!/bin/bash
############################################
#                                          #
#                Grafana                   #
#                                          #
############################################

GRAFANA_CONTAINER="grafana"
GRAFANA_PASSWORD="password"
GRAFANA_PORT="3000"

#Enable this opcions in Centos
#firewall-cmd --permanent --add-port=$GRAFANA_PORT/tcp
#firewall-cmd --reload


docker run -itd --name=$GRAFANA_CONTAINER \
    -p $GRAFANA_PORT:3000 \
    -v /etc/localtime:/etc/localtime:ro \
    -v /usr/share/zoneinfo:/usr/share/zoneinfo:ro \
    -v /Users/mu5t41n3/Documents/docker-compose/Prometheus/archivosBase/grafana:/var/lib/grafana:z \
    -v /Users/mu5t41n3/Documents/docker-compose/Prometheus/archivosBase/grafana-log:/var/log/grafana:z \
    -e "TZ=America/Mexico_City" \
    -e "GF_SECURITY_ADMIN_PASSWORD=$GRAFANA_PASSWORD" \
    grafana/grafana:7.0.0
